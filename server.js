const appointmentDB = require('./src/config/database');
const grpc = require('grpc');
const { 
    getListAppointmentByUser,
    addAppointment,
    UpdateStatusAppointmnent,
    UpdateAppointment
  } = require('./src/service/appointmentServices');
const PROTO_PATH = './src/proto/appointment.proto';
const protoLoader = require('@grpc/proto-loader');
const packageDefinition = protoLoader.loadSync(
    PROTO_PATH,
    {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true
    });
const appointment_proto = grpc.loadPackageDefinition(packageDefinition);
const server = new grpc.Server();
server.addService(appointment_proto.AppointmentService.service, {
    InsertAppointment: addAppointment,
    ListAppointmentByUser: getListAppointmentByUser,
    UpdateStatusAppointmnent: UpdateStatusAppointmnent,
    UpdateAppointment: UpdateAppointment,
})

server.bind('127.0.0.1:50055',
    grpc.ServerCredentials.createInsecure());
console.log('Server running at http://127.0.0.1:50055')
server.start()