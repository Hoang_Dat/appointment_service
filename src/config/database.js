const mongoose = require("mongoose");
const appsettings = require("../property");
const managerDB = mongoose.connection;

mongoose.connect(appsettings.MONGODB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
});
managerDB.once("open", () => {
    console.log("connected to mongo DB successfully!");
}).on("error", (err) => {
    console.log("error: ", err);
});