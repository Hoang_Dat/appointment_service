const { insertAppointment,
    getAllAppointment,
    getAllAppointmentByUser,
    changeStatusAppointmnent,
    updateAppointmnentByAccount } = require('../handler/appointmentHandler');
const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');
const getListAppointmentByUser = async (call, callback) => {
    const listAppointmentByUser = await getAllAppointmentByUser(call.request.userId, call.request.status, call.request.role)
    console.log("result", listAppointmentByUser)
    if (listAppointmentByUser) {
        callback(null, {appointments : listAppointmentByUser})
    } else {
        callback({
            code: grpc.status.INTERNAL,
            details: "getListAppointmentByUser faillure"
        })
    }
}
const addAppointment = async (call, callback) => {
    try {
        const data = {
            idPlace: call.request.idPlace,
            createBy: call.request.createBy,
            hours: call.request.hours,
            topic: call.request.topic,
            collaborator: call.request.collaborator,
            status: call.request.status,
            dateAppointement: call.request.dateAppointement,
        }
        const result = await insertAppointment(data);
        callback(null, result)
    } catch (error) {
        console.log(error)
        callback({
            code: grpc.status.INTERNAL,
            details: "addAppointment faillure"
        })
    }

}

const UpdateStatusAppointmnent = async (call, callback) => {
    try {
        const result = await changeStatusAppointmnent(call.request.id, call.request.status);
        callback(null, result)
    } catch (error) {
        console.log(error)
        callback({
            code: grpc.status.INTERNAL,
            details: "UpdateStatusAppointmnent faillure"
        })
    }

}

const UpdateAppointment = async (call, callback) => {
    try {
        console.log("call.request.appointment", call.request.appointment)
        console.log("call.request.appointment", call.request.id)
        const result = await updateAppointmnentByAccount(call.request.id, call.request.appointment);
        callback(null, result)
    } catch (error) {
        console.log(error)
        callback({
            code: grpc.status.INTERNAL,
            details: "UpdateAppointment faillure"
        })
    }

}

module.exports = {
    getListAppointmentByUser,
    addAppointment,
    UpdateStatusAppointmnent,
    UpdateAppointment
};