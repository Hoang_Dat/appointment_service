const Appointment = require("../model/appointment");

const insertAppointment = async data => {
    console.log(data)
    const newAppointment  = new Appointment(data);
    return await newAppointment.save();
};

const getAllAppointment = async () => {
    return await Appointment.find();
};

const getAllAppointmentByUser = async (createBy, status, role) => {
    console.log(createBy)
    console.log(status)
    console.log(role)
    if(role === "client") 
        return await Appointment.find({ createBy : createBy, status: { $lte: status}})
    else
        return await Appointment.find({ collaborator: createBy , status: { $lte: status}})
};

const changeStatusAppointmnent = async (id, status) => {
    return await Appointment.findByIdAndUpdate(id, {status});
}

const updateAppointmnentByAccount = async (id, appointment) => {
    return await Appointment.findOneAndUpdate({ _id: id}, appointment,{new: true});
};

module.exports = {
    insertAppointment,
    getAllAppointment,
    getAllAppointmentByUser,
    changeStatusAppointmnent,
    updateAppointmnentByAccount
  };