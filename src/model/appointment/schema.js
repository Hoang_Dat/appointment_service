const Schema = require("mongoose").Schema;

const AppointmentSchema = new Schema({
    idPlace: { type: String, required: true },
    createBy:  { type: String, default : 0 },
    hours: { type: Number, default: 0 },
    topic: { type: String, required: true },
    collaborator: { type: String, required: true },
    status: { type: Number },   // 1: Created 2:Approved 3:WaitingForRemovel 4: Completely
    createdDate: { type: Date, default: Date.now },
    dateAppointement: { type: Date, required: true}
  });
  
  module.exports = AppointmentSchema;