const mongoose = require("mongoose");

module.exports = mongoose.model("Appointment", require("./schema"));